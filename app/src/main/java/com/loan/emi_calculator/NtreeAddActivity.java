package com.loan.emi_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.loan.emi_calculator.model.DBHelper;
import com.loan.emi_calculator.model.Ntree;

import java.util.Calendar;

public class NtreeAddActivity extends AppCompatActivity implements View.OnClickListener {

    TextView btTransactionDate;
    EditText etEntryAmount;
    Button btEntryAdd, btGoBack;
    RadioButton rbDisbursal, rbDeposit;
    RadioGroup rg;
    DBHelper dbHelper;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ntree_add);
        dbHelper=new DBHelper(getApplicationContext());
        rg = findViewById(R.id.radioGroup);
        etEntryAmount = findViewById(R.id.editTextEntryAmount);
        btTransactionDate = findViewById(R.id.buttonTransactionDate);
        btEntryAdd = findViewById(R.id.buttonEntryAdd);
        btGoBack = findViewById(R.id.buttonGoBack2);
        rbDisbursal = findViewById(R.id.radioButtonDisbursal);
        rbDeposit = findViewById(R.id.radioButtonDeposit);

        btEntryAdd.setOnClickListener(this);
        btTransactionDate.setOnClickListener(this);
        btGoBack.setOnClickListener(this);
    }

    private boolean CheckEmptyValidation() {
        if(etEntryAmount.length() == 0){
            etEntryAmount.setError("Input transaction amount");
            return false;
        }
        else if(btTransactionDate.getHint().length() != 10){
            btTransactionDate.setError("Select the transaction date");
            return false;
        }
        else if(!rbDisbursal.isChecked() && !rbDeposit.isChecked()) {
            rbDeposit.setError("Pick one category");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if(R.id.buttonEntryAdd == view.getId()) {
            boolean isAllFieldsEntered = CheckEmptyValidation();
            if (isAllFieldsEntered) {
                String entryType = "";
                if (rbDisbursal.isChecked()) {
                    entryType = DBHelper.type_disbursed;
                } else if (rbDeposit.isChecked()) {
                    entryType = DBHelper.type_deposit;
                }
                Ntree ntree = new Ntree(0, btTransactionDate.getHint().toString(), entryType,
                        Double.valueOf(etEntryAmount.getText().toString()));
                dbHelper.InsertData(ntree);
                btEntryAdd.setEnabled(false);

                Intent i = new Intent();
                i.putExtra("ntree", ntree);
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        }
        else if(R.id.buttonGoBack2 == view.getId()) {
            setResult(Activity.RESULT_CANCELED);
            finish();
        }

        if(R.id.buttonTransactionDate == view.getId()) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR); // current year
            int mMonth = c.get(Calendar.MONTH); // current month
            int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
            datePickerDialog = new DatePickerDialog(NtreeAddActivity.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker datePicker, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String m, d;
                            if(monthOfYear+1 < 10) {
                                m = "0" + (monthOfYear+1);
                            }
                            else {
                                m = "" + (monthOfYear+1);
                            }
                            if(dayOfMonth < 10) {
                                d = "0" + dayOfMonth;
                            }
                            else {
                                d = "" + dayOfMonth;
                            }
                            String date =  d + "-" + m + "-" + year;
                            btTransactionDate.setHint(date);
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
    }
}