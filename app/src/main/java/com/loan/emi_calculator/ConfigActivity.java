package com.loan.emi_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loan.emi_calculator.model.DBHelper;

import java.util.Calendar;

public class ConfigActivity extends AppCompatActivity implements View.OnClickListener{

    EditText etEmiMonths;
    Button tvMT, btMT2, finishBT, btEmi;
    TextView tvEmi;
    DatePickerDialog datePickerDialog;
    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        mPrefs = this.getSharedPreferences(DBHelper.spFileName, Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
        etEmiMonths = findViewById(R.id.et_emi);
        tvMT = findViewById(R.id.btMT);
        tvMT.setOnClickListener(this);
        tvEmi = findViewById(R.id.tv_emimonths);
        btEmi = findViewById(R.id.bt_emi);
        btEmi.setOnClickListener(this);
        btMT2 = findViewById(R.id.btMT2);
        btMT2.setOnClickListener(this);
        finishBT = findViewById(R.id.btGoBack);
        finishBT.setOnClickListener(this);
        String moratoriumDate = mPrefs.getString(DBHelper.moratoriumSP, DBHelper.moratorium_till_show);
        tvMT.setHint(moratoriumDate);
        String statusDate = mPrefs.getString(DBHelper.statusDateSP, DBHelper.status_date_show);
        btMT2.setHint(statusDate);

        int monthsSP = mPrefs.getInt(DBHelper.monthsSP, 12);
        tvEmi.setText(String.valueOf(monthsSP));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btMT) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR); // current year
            int mMonth = c.get(Calendar.MONTH); // current month
            int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
            datePickerDialog = new DatePickerDialog(ConfigActivity.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String m, d;
                            if (monthOfYear + 1 < 10) {
                                m = "0" + (monthOfYear + 1);
                            } else {
                                m = "" + (monthOfYear + 1);
                            }
                            if (dayOfMonth < 10) {
                                d = "0" + dayOfMonth;
                            } else {
                                d = "" + dayOfMonth;
                            }
                            String date =  d + "-" + m + "-" + year;
                            tvMT.setHint(date);
                            mEditor.putString(DBHelper.moratoriumSP, date).apply();
                            Toast.makeText(ConfigActivity.this, R.string.moratorium_set, Toast.LENGTH_LONG).show();
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        else if(v.getId() == R.id.btMT2) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR); // current year
            int mMonth = c.get(Calendar.MONTH); // current month
            int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
            datePickerDialog = new DatePickerDialog(ConfigActivity.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String m, d;
                            if (monthOfYear + 1 < 10) {
                                m = "0" + (monthOfYear + 1);
                            } else {
                                m = "" + (monthOfYear + 1);
                            }
                            if (dayOfMonth < 10) {
                                d = "0" + dayOfMonth;
                            } else {
                                d = "" + dayOfMonth;
                            }
                            String date =  d + "-" + m + "-" + year;
                            btMT2.setHint(date);

                            mEditor.putString(DBHelper.statusDateSP, date).apply();
                            Toast.makeText(ConfigActivity.this, R.string.status_date_set, Toast.LENGTH_LONG).show();
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        else if(v.getId() == R.id.btGoBack) {
            setResult(Activity.RESULT_OK);
            finish();
        }
        else if (v.getId() == R.id.bt_emi) {
            String mStr = etEmiMonths.getText().toString();
            tvEmi.setText(mStr);
            if(!"".equals(mStr)) {
                int m = Integer.parseInt(mStr);
                mEditor.putInt(DBHelper.monthsSP, m).apply();
                ;
            }
        }
    }
}