package com.loan.emi_calculator;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class Utils {

    public static String convertToCurrency(String amount) {
        Double amountD = Double.valueOf(amount);
        return convertToCurrency(amountD);
    }

    public static String convertToCurrency(Double amountD) {
        Locale india = new Locale("en", "IN");
        Currency inr = Currency.getInstance(india);
        NumberFormat inrFormat = NumberFormat.getCurrencyInstance(india);
        return inrFormat.format(amountD);
    }

    public static String getFormattedDate(String date) {
        String day = date.substring(8,10);
        String month = date.substring(5,7);
        String year = date.substring(0,4);
        String dateToShow = day + "-" + month + "-" +year;
        return dateToShow;
    }
}
