package com.loan.emi_calculator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.loan.emi_calculator.model.DBHelper;
import com.loan.emi_calculator.model.Ntree;
import com.loan.emi_calculator.model.NtreeAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener {

    private List<Ntree> karjaList = new ArrayList<>();
    NtreeAdapter adapter;
    RecyclerView recyclerView;
    DBHelper dbHelper;
    TextView tvTotal, tvDisbursed, tvDeposited, tvInterest, tvAsOfDate, tvMoratoriumTill, tvSI, tvCI;
    TextView tvEmi;
    private static final int RESULT_MA = 9991;
    private static final int RESULT_IRA = 9992;
    private static final int RESULT_ATA = 9993;
    SharedPreferences sharedPref;
    BottomNavigationView bottomNavigation;
    SharedPreferences.Editor mSPEditor;
    final String[] name = new String[1];
    EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ntree_list);
        tvTotal = findViewById(R.id.tv_total_amount);
        tvDisbursed = findViewById(R.id.tv_disbursed);
        tvDeposited = findViewById(R.id.tv_deposited);
        tvInterest = findViewById(R.id.tv_interest);
        tvAsOfDate = findViewById(R.id.tv_asOfDate);
        tvMoratoriumTill = findViewById(R.id.tv_moratoriumdate);
        tvSI = findViewById(R.id.tv_simpleinterest);
        tvCI = findViewById(R.id.tv_compoundinterest);
        tvEmi = findViewById(R.id.tv_emi);
        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnItemSelectedListener(this);
        dbHelper=new DBHelper(getApplicationContext());
        sharedPref = this.getSharedPreferences(DBHelper.spFileName, Context.MODE_PRIVATE);
        mSPEditor = sharedPref.edit();

        karjaList.addAll(dbHelper.ReadData());
        Collections.sort(karjaList);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapter = new NtreeAdapter(karjaList);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        bottomNavigation.setSelectedItemId(R.id.mainscreen_item);
        enableSwipeToDeleteAndUndo();

        setImportantDates();

        karjaList = dbHelper.easyCompute();
        setWidgets(karjaList);

        input = new EditText(this);
        input.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.action_instructions) {
            Intent i = new Intent(MainActivity.this, InstructionsActivity.class);
            startActivity(i);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                final int position = viewHolder.getAbsoluteAdapterPosition();
                final Ntree item = adapter.getData().get(position);
                if(dbHelper.deleteNtreeRecord(item)) {
                    karjaList = dbHelper.easyCompute();
                    adapter.removeItem(position);
                    setWidgets(karjaList);
                };
            }
        };
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }

    public void setImportantDates() {
        String moratoriumDate = sharedPref.getString(DBHelper.moratoriumSP, DBHelper.moratorium_till_show);
        dbHelper.setMoratoriumUsingSP(moratoriumDate);
        if(!moratoriumDate.equals(DBHelper.moratorium_till_show)) {
            tvMoratoriumTill.setText(moratoriumDate);
        }

        String statusDate = sharedPref.getString(DBHelper.statusDateSP, DBHelper.status_date_show);
        if(statusDate.equals(DBHelper.status_date_show)) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            java.util.Date todayDate = new Date();
            statusDate = formatter.format(todayDate);
            statusDate = Utils.getFormattedDate(statusDate);
        }
//        Log.e("status date:", statusDate);
        dbHelper.setStatusDateUsingSP(statusDate);
        tvAsOfDate.setText(statusDate);

        int monthsSP = sharedPref.getInt(DBHelper.monthsSP, 12);
        dbHelper.setMonthsEmiCalculationSP(monthsSP);
    }

    public void setWidgets(List<Ntree> karjaList) {
        Collections.sort(karjaList);
        adapter.upgradeNtreeList(karjaList);

        tvTotal.setText(Utils.convertToCurrency(dbHelper.t_totalOwed));
        tvDisbursed.setText(Utils.convertToCurrency(dbHelper.t_disbursed));
        tvDeposited.setText(Utils.convertToCurrency(dbHelper.t_deposited));
        tvInterest.setText(Utils.convertToCurrency(dbHelper.t_totalInterest));
        tvSI.setText(Utils.convertToCurrency(dbHelper.t_simpleInterest));
        tvCI.setText(Utils.convertToCurrency(dbHelper.t_compoundInterest));
        setEmiForN();
    }

    private void setEmiForN() {
        String emiForN1 = "Pay " + Utils.convertToCurrency(dbHelper.t_emiCalculation)
                + " EMI for " + DBHelper.months_emic + " months to repay completely.";
        tvEmi.setText(emiForN1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bottomNavigation.setSelectedItemId(R.id.mainscreen_item);
        if(resultCode == RESULT_OK) {
            if(requestCode == RESULT_MA) {
                setImportantDates();
            }
            else if(requestCode == RESULT_IRA) {
            }
            else if(requestCode == RESULT_ATA) {
                if(data != null) {
                    Ntree ntree = (Ntree) data.getSerializableExtra("ntree");
                    String d = dbHelper.dateToSqliteDate(ntree.getProcessingDate());
                    ntree.setProcessingDate(d);
                    karjaList.add(ntree);
                    Collections.sort(karjaList);
                    int sortedIndex = karjaList.indexOf(ntree);
                    adapter.updateNtreeList(karjaList, sortedIndex);
                }
            }
        }
        else if(resultCode == RESULT_CANCELED) {
            if(requestCode == RESULT_ATA) {
//                Toast.makeText(MainActivity.this, "No change done", Toast.LENGTH_SHORT).show();
            }
            else if(requestCode == RESULT_IRA) {
            }
            else if(requestCode == RESULT_MA) {
            }
        }
        karjaList = dbHelper.easyCompute();
        setWidgets(karjaList);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if(item.getItemId() == R.id.mainscreen_item) {
                return true;
            }
            else if(item.getItemId() == R.id.add_ntree_item) {
                Intent intent = new Intent(MainActivity.this, NtreeAddActivity.class);
                startActivityIfNeeded(intent, RESULT_ATA);
                return true;
            }
            else if(item.getItemId() == R.id.interest_rate_item) {
                Intent intent = new Intent(MainActivity.this, InterestRateActivity.class);
                startActivityIfNeeded(intent, RESULT_IRA);
                return true;
            }
            else if(item.getItemId() == R.id.moratorium_item) {
                Intent intent = new Intent(MainActivity.this, ConfigActivity.class);
                startActivityIfNeeded(intent, RESULT_MA);
                return true;
            }
        return false;
    }
}
