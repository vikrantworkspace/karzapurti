package com.loan.emi_calculator.model;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.loan.emi_calculator.R;
import com.loan.emi_calculator.Utils;

import java.util.List;

public class NtreeAdapter extends RecyclerView.Adapter<NtreeAdapter.EntryViewHolder> {

    private List<Ntree> ntreeList;

    public NtreeAdapter(List<Ntree> list) {
        this.ntreeList = list;
    }

    @NonNull
    @Override
    public EntryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.listitem_ntree, parent, false);
        return new EntryViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull EntryViewHolder holder, int position) {
        String processingDate = String.valueOf(ntreeList.get(position).getProcessingDate());
        holder.tvEntryDate.setText(Utils.getFormattedDate(processingDate));
        String amount = String.valueOf(ntreeList.get(position).getAmount());
        String formattedAmount = Utils.convertToCurrency(amount);
        String txnType = String.valueOf(ntreeList.get(position).getNtreeType());
        holder.tvEntryAmount.setText(formattedAmount);
        if(DBHelper.type_disbursed.equals(txnType)) {
            holder.tvEntryAmount.setTextColor(Color.RED);
        }
        else if(DBHelper.type_deposit.equals(txnType)) {
            holder.tvEntryAmount.setTextColor(Color.GREEN);
        }
        holder.tvEntryType.setText(txnType);
        String formattedInterestAmount = Utils.convertToCurrency(ntreeList.get(position).getInterest());
        holder.tvNtreeInterest.setText(formattedInterestAmount);
        String interestDays = String.valueOf(ntreeList.get(position).getInterestDays()) + " days";
        holder.tvNtreeInterestDays.setText(interestDays);
        Log.e("NTREE", formattedAmount);
    }

    public void updateNtreeList(List<Ntree> ntreeL, int index) {
        this.ntreeList = ntreeL;
        notifyItemInserted(index);
    }

    public void upgradeNtreeList(List<Ntree> ntreeL) {
//        for(Ntree ntree : this.ntreeList) {
//            ntree.getId()
//        }
        this.ntreeList = ntreeL;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        this.ntreeList.remove(position);
        notifyItemRemoved(position);
    }

    public List<Ntree> getData() {
        return ntreeList;
    }

    @Override
    public int getItemCount() {
        return ntreeList.size();
    }

    static class EntryViewHolder extends RecyclerView.ViewHolder {

        TextView tvEntryDate, tvEntryAmount, tvEntryType, tvNtreeInterest, tvNtreeInterestDays;

        public EntryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEntryDate = itemView.findViewById(R.id.tv_entry_date);
            tvEntryAmount = itemView.findViewById(R.id.tv_entry_amount);
            tvEntryType = itemView.findViewById(R.id.tv_entry_type);
            tvNtreeInterest = itemView.findViewById(R.id.tv_entry_interest);
            tvNtreeInterestDays = itemView.findViewById(R.id.tv_entry_interest_days);
        }
    }
}
