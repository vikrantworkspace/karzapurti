package com.loan.emi_calculator.model;


import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Ntree implements Serializable, Comparable<Ntree> {

    private int id;
    private String processingDate;
    private String ntreeType;
    private double amount, interest;
    private long interestDays;

    public Ntree(int id, String processingDate, String ntreeType, double amount) {
        this.id = id;
        this.processingDate = processingDate;
        this.ntreeType = ntreeType;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(String processingDate) {
        this.processingDate = processingDate;
    }

    public String getNtreeType() {
        return ntreeType;
    }

    public void setNtreeType(String ntreeType) {
        this.ntreeType = ntreeType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public int compareTo(Ntree o) {
        String date2 = o.getProcessingDate();
        String date1 = this.processingDate;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            java.util.Date d1 = formatter.parse(date1);
            java.util.Date d2 = formatter.parse(date2);
            if(d1 != null && d2 != null) {
                if (d1.getTime() > d2.getTime()) {
                    return -1;
                } else if (d1.getTime() == d2.getTime()) {
                    return -1;
                } else {
                    return 1;
                }
            }
            else {
                Log.e("Ntree", "Date Format exception");
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public long getInterestDays() {
        return interestDays;
    }

    public void setInterestDays(long interestDays) {
        this.interestDays = interestDays;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }
}
