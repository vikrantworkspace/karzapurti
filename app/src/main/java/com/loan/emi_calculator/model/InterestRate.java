package com.loan.emi_calculator.model;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class InterestRate implements Comparable<InterestRate>{

    private String startDate;
    private String endDate;
    private Double interestRateValue;
    private boolean currentValue;

    public InterestRate(String startDate, String endDate, double interestRateValue) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.interestRateValue = interestRateValue;
        if(this.endDate == null) {
            this.currentValue = true;
        }
        else {
            this.currentValue = false;
        }
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Double getInterestRateValue() {
        return interestRateValue;
    }

    public void setInterestRateValue(Double interestRateValue) {
        this.interestRateValue = interestRateValue;
    }

    public boolean getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(boolean currentValue) {
        this.currentValue = currentValue;
    }

    @Override
    public int compareTo(InterestRate o) {
        String date2 = o.getStartDate();
        String date1 = this.startDate;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            java.util.Date d1 = formatter.parse(date1);
            java.util.Date d2 = formatter.parse(date2);
            if(d1 != null && d2 != null) {
                if (d1.getTime() > d2.getTime()) {
                    return -1;
                } else if (d1.getTime() == d2.getTime()) {
                    return -1;
                } else {
                    return 1;
                }
            }
            else {
                Log.e("Ntree", "Date Format exception");
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
