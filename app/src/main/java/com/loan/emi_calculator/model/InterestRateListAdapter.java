package com.loan.emi_calculator.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.loan.emi_calculator.R;

import java.util.List;

public class InterestRateListAdapter extends RecyclerView.Adapter<InterestRateListAdapter.InterestRateViewHolder>{

    private List<InterestRate> irList;
    public InterestRateListAdapter(List<InterestRate> list) {
        this.irList = list;
    }

    @NonNull
    @Override
    public InterestRateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.listitem_ir, parent, false);
        return new InterestRateListAdapter.InterestRateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InterestRateViewHolder holder, int position) {
        holder.tvStartDate.setText(getFormattedDate(String.valueOf(irList.get(position).getStartDate())));
        holder.tvEndDate.setText(getFormattedDate(String.valueOf(irList.get(position).getEndDate())));
        String irP = String.valueOf(irList.get(position).getInterestRateValue()) + "%";
        holder.tvIRValue.setText(irP);
    }

    private String getFormattedDate(String date) {
        String processingDate = String.valueOf(date);
        String day = processingDate.substring(8,10);
        String month = processingDate.substring(5,7);
        String year = processingDate.substring(0,4);
        String dateToShow = day + "-" + month + "-" +year;
        return dateToShow;
    }

    public void removeItem(int position) {
        irList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(InterestRate item, int position) {
        irList.add(position, item);
        notifyItemInserted(position);
    }

    public List<InterestRate> getData() {
        return irList;
    }

    public void addItem(int index) {
        notifyItemInserted(index);
    }

    @Override
    public int getItemCount() {
        return irList.size();
    }

    class InterestRateViewHolder extends RecyclerView.ViewHolder {
        TextView tvStartDate, tvEndDate, tvIRValue;
        public InterestRateViewHolder(@NonNull View itemView) {
            super(itemView);
            tvStartDate = itemView.findViewById(R.id.tv_ir_startdate_rl);
            tvEndDate = itemView.findViewById(R.id.tv_ir_enddate_rl);
            tvIRValue = itemView.findViewById(R.id.tv_ir_value_rl);
        }
    }
}
