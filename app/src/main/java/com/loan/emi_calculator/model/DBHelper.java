package com.loan.emi_calculator.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DBHelper extends SQLiteOpenHelper {

    public static String DBNAME="Database.db";
    public static String TABLE_NAME="ntree";
    public static String TABLE_INTEREST_RATE = "interest_rate";
    public static String moratoriumSP = "sp_moratorium_date";
    public static String statusDateSP = "sp_status_date";
    public static String monthsSP = "sp_months_emi_c";
    public static String spFileName = "app_preferences";
    public static String type_disbursed = "Disbursed";
    public static String type_deposit = "Deposited";
    public static String moratorium_till = "2010-01-01";
    public static String status_date = "2010-01-01";
    public static String moratorium_till_show = "01-01-2010";
    public static String status_date_show = "01-01-2010";
    public static int months_emic = 0;
    public double t_disbursed = 0;
    public double t_deposited = 0;
    public double t_totalOwed = 0;
    public double t_totalInterest = 0;
    public double t_simpleInterest = 0;
    public double t_compoundInterest = 0;
    public double t_interestRateOnStatusDate = 0;
    public double t_emiCalculation = 0;
    Context activity;

    public DBHelper(@Nullable Context context) {
        super(context, DBNAME, null, 1);
        activity=context;
    }

    public void setMoratoriumUsingSP(String moratoriumDate) {
        moratorium_till = dateToSqliteDate(moratoriumDate);
    }

    public void setStatusDateUsingSP(String statusDate) {
        status_date = dateToSqliteDate(statusDate);
    }

    public void setMonthsEmiCalculationSP(int n) {
        months_emic = n;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query="CREATE TABLE "+TABLE_NAME+"(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "processingDate DATE,amount NUMBER,type TEXT)";
        sqLiteDatabase.execSQL(query);

        String query2 = "CREATE TABLE "+TABLE_INTEREST_RATE +"(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "startDate DATE,endDate Date,interestRateValue NUMBER)";
        sqLiteDatabase.execSQL(query2);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public void InsertData(Ntree ntree) {
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        String dateSqliteFormat = dateToSqliteDate(ntree.getProcessingDate());
        contentValues.put("processingDate",dateSqliteFormat);
        contentValues.put("amount",ntree.getAmount());
        contentValues.put("type", ntree.getNtreeType());
        long result  =  sqLiteDatabase.insert(TABLE_NAME,null,contentValues);
//        Toast.makeText(activity, ""+result, Toast.LENGTH_SHORT).show();
    }

    public String dateToSqliteDate(String dateString) {
        String sqliteDate = "";
        String day = dateString.substring(0,2);
        String month = dateString.substring(3,5);
        String year = dateString.substring(6,10);
        sqliteDate = year + "-" + month + "-" + day;
        return sqliteDate;
    }

    public void InsertInterestRateData(InterestRate interestRate) {
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String startDateSqliteFormat = dateToSqliteDate(interestRate.getStartDate());
        String endDateSqliteFormat = dateToSqliteDate(interestRate.getEndDate());
        contentValues.put("startDate",startDateSqliteFormat);
        contentValues.put("endDate",endDateSqliteFormat);
        contentValues.put("interestRateValue", interestRate.getInterestRateValue());

        long result  =  sqLiteDatabase.insert(TABLE_INTEREST_RATE,null,contentValues);
//        Toast.makeText(activity, ""+result, Toast.LENGTH_SHORT).show();
        sqLiteDatabase.close();
    }

    public List<Ntree> ReadData() {
        List<Ntree> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select * from ntree";

        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()) {
            do{
                String processingDate = cursor.getString(cursor.getColumnIndexOrThrow("processingDate"));
                Long amount = cursor.getLong(cursor.getColumnIndexOrThrow("amount"));
                String entryType = cursor.getString(cursor.getColumnIndexOrThrow("type"));
                int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                Ntree ntree = new Ntree(id, processingDate, entryType, amount);
                list.add(ntree);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public boolean deleteInterestRateRecord(String startDate, String endDate) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        int index = sqLiteDatabase.delete(TABLE_INTEREST_RATE, "startDate=?", new String[]{startDate});
        sqLiteDatabase.close();
        return index > 0;
    }

    public boolean deleteNtreeRecord(Ntree ntree) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        int index = sqLiteDatabase.delete(TABLE_NAME, "id=?", new String[]{Integer.toString(ntree.getId())});
        sqLiteDatabase.close();
        return index > 0;
    }

    public List<InterestRate> ReadInterestRateData() {
        List<InterestRate> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select * from interest_rate order by startDate desc";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()) {
            do{
                String startDate = cursor.getString(cursor.getColumnIndexOrThrow("startDate"));
                String endDate = cursor.getString(cursor.getColumnIndexOrThrow("endDate"));
                Double interestRateValue = cursor.getDouble(cursor.getColumnIndexOrThrow("interestRateValue"));
                InterestRate interestRate = new InterestRate(startDate, endDate, interestRateValue);
                list.add(interestRate);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    double f = 1;
    double f_si = 1;
    double txnComponent = 0;
    double txnSIComponent = 0;
    public List<Ntree> easyCompute() {
        txnComponent = 0;
        txnSIComponent = 0;
        t_disbursed = 0;
        t_deposited = 0;
        t_simpleInterest = 0;
        List<Ntree> entries = ReadData();
        for(Ntree ntree : entries) {
            f = 1;
            f_si = 1;
            double amount = ntree.getAmount();
            String type = ntree.getNtreeType();
//            Log.e("DBHelper: ", "======================================");
//            Log.e("DBHelper amount: ", String.valueOf(amount));
//            Log.e("DBHelper type: ", type);
            String processingDate = ntree.getProcessingDate();
//            Log.e("DBHelper D:", processingDate);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            try {
                java.util.Date transactionDate = formatter.parse(processingDate);
                java.util.Date statusDate = formatter.parse(status_date);
//                Log.e("DBHelper status date:", status_date);
                java.util.Date mDate = formatter.parse(moratorium_till);
//                Log.e("DBHelper moratorium:", moratorium_till);

                long d_transactionDate = transactionDate.getTime();
                long d_statusDate = statusDate.getTime();
                long d_moratoriumDate = mDate.getTime();

                if(d_statusDate >= d_transactionDate) {
                    Long interestDays = TimeUnit.DAYS.convert(d_statusDate - d_transactionDate
                            , TimeUnit.MILLISECONDS);
                    ntree.setInterestDays(interestDays);
                }

                List<InterestRate> interestRateList = ReadInterestRateData();
                double s = 1;
                double c = 1;
                for(InterestRate interestRate : interestRateList) {

                    String startDate = interestRate.getStartDate();
                    String endDate = interestRate.getEndDate();
                    Double interestRateValue = interestRate.getInterestRateValue();
                    java.util.Date date2 = formatter.parse(startDate);
                    java.util.Date date1 = formatter.parse(endDate);
                    long d_endDate = date1.getTime();
                    long d_startDate = date2.getTime();
                    // single interest rate to consider to calculate emi
                    if(d_startDate <= d_statusDate && d_statusDate <= d_endDate) {
                        t_interestRateOnStatusDate = interestRateValue;
                    }
                    if(d_statusDate >= d_startDate && d_transactionDate <= d_statusDate) {
//                        Log.e("DBHelper start", startDate);
                        if (d_transactionDate <= d_endDate) {

                            if (d_statusDate > d_endDate) {
                                d_statusDate = d_endDate;
                            }
                            if (d_transactionDate < d_startDate) {
                                d_transactionDate = d_startDate;
                            }

                            if (d_moratoriumDate < d_startDate) {
                                //compounding
                                long diff1 = d_statusDate - d_transactionDate;
                                float days1 = TimeUnit.DAYS.convert(diff1, TimeUnit.MILLISECONDS);
//                                Log.e("DBHelper c1 days:", String.valueOf(days1));
                                double compoundingMultiplier =
                                        Math.pow((1 + interestRateValue / (12 * 100)), days1 * 12 / 365);
//                                Log.e("DBHelper c1:", String.valueOf(compoundingMultiplier));
                                c *= compoundingMultiplier;
                            } else if (d_endDate >= d_moratoriumDate) {
                                if (d_moratoriumDate > d_statusDate) {
                                    //moratorium ends after today, SI
                                    long diff1 = d_transactionDate - d_statusDate;
                                    float days1 = TimeUnit.DAYS.convert(diff1, TimeUnit.MILLISECONDS);
                                    double simpleInterestComponent = (days1 * interestRateValue) / (365 * 100);
//                                    Log.e("DBHelper s1:", String.valueOf(simpleInterestComponent));
                                    s += simpleInterestComponent;
                                } else if (d_transactionDate > d_moratoriumDate) {
                                    //compound
                                    long diff1 = d_statusDate - d_transactionDate;
                                    float days1 = TimeUnit.DAYS.convert(diff1, TimeUnit.MILLISECONDS);
                                    double compoundingMultiplier =
                                            Math.pow((1 + interestRateValue / (12 * 100)), days1 * 12 / 365);
//                                    Log.e("DBHelper c2:", String.valueOf(compoundingMultiplier));
                                    c *= compoundingMultiplier;
                                } else {
                                    // transactionDate->m simple, m -> statusDate compounding
                                    long diff1 = d_statusDate - d_moratoriumDate;
                                    float days1 = TimeUnit.DAYS.convert(diff1, TimeUnit.MILLISECONDS);
                                    double compoundingMultiplier = Math.pow((1 + interestRateValue / (12 * 100)), days1 * 12 / 365);
//                                    Log.e("DBHelper c3:", String.valueOf(compoundingMultiplier));
                                    c *= compoundingMultiplier;

                                    long diff2 = d_moratoriumDate - d_transactionDate;
                                    float days2 = TimeUnit.DAYS.convert(diff2, TimeUnit.MILLISECONDS);
                                    double simpleInterestComponent = (days2 * interestRateValue) / (365 * 100);
//                                    Log.e("DBHelper s2:", String.valueOf(simpleInterestComponent));
                                    s += simpleInterestComponent;
                                }
                            } else {
                                //simple interest
                                long diff1 = d_statusDate - d_transactionDate;
                                float days1 = TimeUnit.DAYS.convert(diff1, TimeUnit.MILLISECONDS);
                                double simpleInterestComponent = (days1 * interestRateValue) / (365 * 100);
//                                Log.e("DBHelper s3:", String.valueOf(simpleInterestComponent));
                                s += simpleInterestComponent;
                            }
                        }
                    }
                }
                f = s*c;
                f_si = s-1;
                if(type_disbursed.equals(type)) {
                    double interestComponent = amount * (f-1);
                    txnComponent += amount * f;
                    txnSIComponent += amount * f_si;
                    t_disbursed += amount;
                    ntree.setInterest(interestComponent);
                }
                else if(type_deposit.equals(type)) {
                    f = f*(-1);
                    double interestComponent = amount * (f+1);
                    txnComponent += amount * f;
                    txnSIComponent += amount * f_si;
                    t_deposited += amount;
                    ntree.setInterest(interestComponent);
                }
            }
            catch (ParseException e){
//                Log.e("DBHelper", e.toString());
            }
//            Log.e("DBHelper c", String.valueOf(amount*f));
//            Log.e("DBHelper owed", String.valueOf(txnComponent));
        }
        t_simpleInterest = txnSIComponent;
        t_totalOwed = txnComponent;
        t_totalInterest = t_totalOwed + t_deposited - t_disbursed;
        t_compoundInterest = t_totalInterest - t_simpleInterest;
        computeEMI();
        return entries;
    }

    public void computeEMI() {
        t_emiCalculation = 0;
        if(t_interestRateOnStatusDate >= 0) {
            double factorForEMI = 0;
            double iPlusOne = 1 + t_interestRateOnStatusDate / (12 * 100);
            for(float i=1; i <= months_emic; i++) {
                factorForEMI += Math.pow(iPlusOne, -i/12);
            }
            t_emiCalculation = t_totalOwed/factorForEMI;
        }
    }
}
