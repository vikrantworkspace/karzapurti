package com.loan.emi_calculator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.loan.emi_calculator.model.DBHelper;
import com.loan.emi_calculator.model.InterestRate;
import com.loan.emi_calculator.model.InterestRateListAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class InterestRateActivity extends AppCompatActivity implements View.OnClickListener {

    Button btStartDate, btEndDate, btGoBack;
    EditText etIRValue;
    Button btAddRate;
    DatePickerDialog datePickerDialog;
    boolean isAllFieldsEntered = false;
    DBHelper dbHelper;
    RecyclerView recyclerViewIR;
    List<InterestRate> irList = new ArrayList<>();
    InterestRateListAdapter irAdapter;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_rate);

        btStartDate = findViewById(R.id.buttonStartDate);
        btEndDate = findViewById(R.id.buttonEndDate);
        etIRValue = findViewById(R.id.editTextRate);
        btAddRate = findViewById(R.id.buttonAddRate);
        btGoBack = findViewById(R.id.buttonGoBack);
        recyclerViewIR = findViewById(R.id.recycler_view_ir);

        dbHelper=new DBHelper(getApplicationContext());
        irList.addAll(dbHelper.ReadInterestRateData());
        Collections.sort(irList);
        irAdapter = new InterestRateListAdapter(irList);

        RecyclerView.LayoutManager lm = new LinearLayoutManager(InterestRateActivity.this);
        recyclerViewIR.setLayoutManager(lm);
        recyclerViewIR.setAdapter(irAdapter);
//        recyclerViewIR.addItemDecoration(new DividerItemDecoration(recyclerViewIR.getContext(), DividerItemDecoration.VERTICAL));

        btStartDate.setOnClickListener(this);
        btEndDate.setOnClickListener(this);
        btAddRate.setOnClickListener(this);
        btGoBack.setOnClickListener(this);

        enableSwipeToDeleteAndUndo();

    }

    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                final int position = viewHolder.getAbsoluteAdapterPosition();
                final InterestRate item = irAdapter.getData().get(position);
                if(dbHelper.deleteInterestRateRecord(item.getStartDate(), item.getEndDate())) {
                    dbHelper.easyCompute();
                    irAdapter.removeItem(position);
                }
            }
        };
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerViewIR);
    }

    private boolean CheckEmptyValidation() {
        if(etIRValue.length() == 0){
            etIRValue.setError("Input Interest Rate");
            return false;
        }
        else if(btStartDate.getHint().length() != 10){
            btStartDate.setError("Choose a Start Date");
            return false;
        }
        else if(btEndDate.getHint().length() !=  10) {
            btEndDate.setError("Choose an End Date");
            return false;
        }
        return true;
    }

    public String dateToSqliteDate(String dateString) {
        String sqliteDate = "";
        String day = dateString.substring(0,2);
        String month = dateString.substring(3,5);
        String year = dateString.substring(6,10);
        sqliteDate = year + "-" + month + "-" + day;
        return sqliteDate;
    }

    public boolean isValidDateRange(String enteredStartDate, String enteredEndDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            try {
                java.util.Date enteredDate1 = formatter.parse(dateToSqliteDate(enteredStartDate));
                java.util.Date enteredDate2 = formatter.parse(dateToSqliteDate(enteredEndDate));
                long ed1 = enteredDate1.getTime();
                long ed2 = enteredDate2.getTime();
                if(ed1 >= ed2) {
                    return false;
                }
            }
            catch (ParseException e) {}
        return true;
    }

    public boolean isUniqueDateRange(String enteredStartDate, String enteredEndDate) {
        List<InterestRate> interestRateList = dbHelper.ReadInterestRateData();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        for(InterestRate interestRate : interestRateList) {
            String startDate = interestRate.getStartDate();
            String endDate = interestRate.getEndDate();
            try {
                java.util.Date date1 = formatter.parse(startDate);
                java.util.Date date2 = formatter.parse(endDate);
                java.util.Date enteredDate1 = formatter.parse(dateToSqliteDate(enteredStartDate));
                java.util.Date enteredDate2 = formatter.parse(dateToSqliteDate(enteredEndDate));
                long d1 = date1.getTime();
                long d2 = date2.getTime();
                long ed1 = enteredDate1.getTime();
                long ed2 = enteredDate2.getTime();

                if (ed1 == d1 || ed2 == d1) {
                    return false;
                }
                if (ed1 > d1 && ed1 < d2) {
                    return false;
                } else if (ed2 > d1 && ed2 < d2) {
                    return false;
                }
            }
            catch (ParseException e) {}
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if(R.id.buttonAddRate == v.getId()) {
            isAllFieldsEntered = CheckEmptyValidation();
            if(isAllFieldsEntered) {
                Double irValue = Double.valueOf(etIRValue.getText().toString());
                String startDate = btStartDate.getHint().toString();
                String endDate = btEndDate.getHint().toString();
                InterestRate interestRate = new InterestRate(startDate, endDate, irValue);
                // validation for existing dates and add to the table
                if(!isUniqueDateRange(startDate, endDate)) {
                    Toast.makeText(InterestRateActivity.this, R.string.entry_unique_date_range,
                            Toast.LENGTH_LONG).show();
                }
                else if(!isValidDateRange(startDate, endDate)) {
                    Toast.makeText(InterestRateActivity.this, R.string.entry_invalid_date_range,
                            Toast.LENGTH_LONG).show();
                }
                else {
                    dbHelper.InsertInterestRateData(interestRate);
                    btAddRate.setEnabled(false);
                    irList.add(interestRate);
                    Collections.sort(irList);
                    int sortedIndex = irList.indexOf(interestRate);
                    irAdapter.addItem(sortedIndex);
                    Toast.makeText(InterestRateActivity.this, "Interest Rate Added", Toast.LENGTH_LONG).show();
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
        }
        else if(R.id.buttonStartDate == v.getId()) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR); // current year
            int mMonth = c.get(Calendar.MONTH); // current month
            int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
            datePickerDialog = new DatePickerDialog(InterestRateActivity.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String m, d;
                            if(monthOfYear+1 < 10) {
                                m = "0" + (monthOfYear+1);
                            }
                            else {
                                m = "" + (monthOfYear+1);
                            }
                            if(dayOfMonth < 10) {
                                d = "0" + dayOfMonth;
                            }
                            else {
                                d = "" + dayOfMonth;
                            }
                            String date =  d + "-" + m + "-" + year;
                            btStartDate.setHint(date);
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        else if(R.id.buttonEndDate == v.getId()) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR); // current year
            int mMonth = c.get(Calendar.MONTH); // current month
            int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
            datePickerDialog = new DatePickerDialog(InterestRateActivity.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String m, d;
                            if(monthOfYear+1 < 10) {
                                m = "0" + (monthOfYear+1);
                            }
                            else {
                                m = "" + (monthOfYear+1);
                            }
                            if(dayOfMonth < 10) {
                                d = "0" + dayOfMonth;
                            }
                            else {
                                d = "" + dayOfMonth;
                            }
                            String date =  d + "-" + m + "-" + year;
                            btEndDate.setHint(date);
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        else if (v.getId() == R.id.buttonGoBack) {
            setResult(Activity.RESULT_CANCELED);
            finish();
        }
    }
}